import 'package:redux/redux.dart';
import 'package:code_challenge/entities/entities.dart';
import 'package:code_challenge/models/models.dart';
import 'package:code_challenge/services/services.dart';
import 'package:code_challenge/actions/actions.dart';

List<Middleware<AppState>> createAllMiddlewares() {
  return [
    TypedMiddleware<AppState, LoadAppAction>(_initalizeApp),
    TypedMiddleware<AppState, LoadUserAction>(_loadCurrentUser),
    TypedMiddleware<AppState, LoadMatchAction>(_loadMatchs),
    TypedMiddleware<AppState, LoadStrangersAction>(_loadStrangers),
  ];
}

void _initalizeApp(Store<AppState> store, action, NextDispatcher next) {
  next(action);
  next(new LoadUserAction());
}

void _loadCurrentUser(Store<AppState> store, action, NextDispatcher next) {
  next(action);

  UsersService
    .loadCurrentUser()
    .then((UserEntity data) {
      store.dispatch(new LoadMatchAction());
      store.dispatch(new LoadStrangersAction());
      store.dispatch(new LoadUserSuccessAction(data));
    })
    .catchError((String message) =>
      store.dispatch(new LoadUserFailAction(message)));
}

void _loadMatchs(Store<AppState> store, action, NextDispatcher next) {
  next(action);

  UsersService
    .loadMatchs()
    .then((List<UserEntity> data) =>
      store.dispatch(new LoadMatchSuccessAction(data)))
    .catchError((String message) =>
      store.dispatch(new LoadMatchFailAction(message)));
}

void _loadStrangers(Store<AppState> store, action, NextDispatcher next) {
  next(action);

  UsersService
    .loadStrangers()
    .then((List<UserEntity> data) =>
      store.dispatch(new LoadStrangersSuccessAction(data)))
    .catchError((String message) =>
      store.dispatch(new LoadStrangersFailAction(message)));
}
