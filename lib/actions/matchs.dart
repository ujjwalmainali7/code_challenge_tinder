import 'package:code_challenge/entities/entities.dart';

class LoadMatchAction {}

class LoadMatchSuccessAction {
  final List<UserEntity> matchs;

  LoadMatchSuccessAction(this.matchs);

  @override
  String toString() =>
    'LoadMatchsSuccessAction{matchs: $matchs}';
}

class LoadMatchFailAction {
  final String message;

  LoadMatchFailAction(this.message);

  @override
  String toString() =>
    'LoadMatchsFailAction{message: $message}';
}
