import 'package:redux/redux.dart';

import 'package:code_challenge/actions/actions.dart';
import 'package:code_challenge/models/models.dart';

final matchsReducer = combineReducers<Matchs>([
  new TypedReducer<Matchs, LoadMatchAction>(_loadMatchs),
  new TypedReducer<Matchs, LoadMatchSuccessAction>(_loadMatchsSuccess),
]);

Matchs _loadMatchs(Matchs state, LoadMatchAction action) {
  return new Matchs.loading();
}

Matchs _loadMatchsSuccess(Matchs state, LoadMatchSuccessAction action) {
  return new Matchs(
    isLoading: false,
    matchs: action.matchs,
  );
}
